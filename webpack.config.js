const path = require('path');
const webpack = require('webpack');
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")

module.exports = (env, argv) => { 
  const devMode = argv.mode !== 'production';

  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: devMode ? 'inline-source-map' : undefined,
    entry: {
      web3Wallet: [
        './src/index.js'
      ]
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
      library: {
        name: 'web3Wallet',
        type: 'umd',
      },
      umdNamedDefine: true,
      globalObject: "this",
      clean: true,
    },
    resolve: {
      extensions: ['.js'],
      modules: ['node_modules'],
      fallback: {
        vm: require.resolve('vm-browserify'),
        url: require.resolve('url'),
        path: require.resolve('path-browserify'),
        zlib: require.resolve('browserify-zlib')
      },
      alias: {
        assert: 'assert',
        buffer: 'buffer',
        crypto: 'crypto-browserify',
        http: 'stream-http',
        https: 'https-browserify',
        os: 'os-browserify/browser',
        process: 'process/browser',
        stream: 'stream-browserify',
        util: 'util',
        '@ledgerhq/connect-kit/dist/umd': '@ledgerhq/connect-kit/dist/umd/index.js'
      }
    },
    experiments: {
      asyncWebAssembly: true
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        name: false,
        cacheGroups: {
          default:false
        }
      }
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_DEBUG': false
      }),
      new NodePolyfillPlugin(),
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      })
    ],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: 'babel-loader',
        },
      ],
    }
  }
}
