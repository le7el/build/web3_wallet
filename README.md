# Web3 Wallet

Libary to connect and interact with web3 wallets
Supported wallet providers: metamask, wallet-connect

## Installation

```bash
npm install @le7el/web3_wallet
```

## Getting started
This library exposes multiple publicly accesible methods, which could help in different web3 wallet use cases

##### Typical library usage for connecting a wallet and listen for its changes:
```js
import { useProvider, connectWallet, listenWallet } from '@le7el/web3_wallet'

useProvider('metamask')
  .then(connectWallet)
  .then(connectedWalletAddress => {
    console.log('wallet connection success')
  })
  .then(() => {
    // in case when you need to listen on wallet changes, like: account changed or network changed,
    // you'll also need to call listenWallet with approproate callbacks
    const onWalletChange = (newWalletAddress) => {}
    const onWalletError = (error) => {}

    listenWallet(onWalletChange, onWalletError)
  })
  .catch(error => {
    console.log('wallet connection failed')
  })
```