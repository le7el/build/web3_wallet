import { ethers } from "ethers"

const cidToBytes32 = (cid) => {
  return ethers.utils.hexlify(ethers.utils.base58.decode(cid).slice(2))
}

const bytes32ToCid = (hex) => {
  return ethers.utils.base58.encode(`0x1220${hex.slice(2)}`)
}

export {
  cidToBytes32,
  bytes32ToCid,
}