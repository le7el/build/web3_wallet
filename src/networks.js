import { utils } from "ethers"

const NETWORKS = {
  1: "ethereum",
  3: "ropsten",
  4: "rinkeby",
  5: "goerli",
  42: "kovan",
  11155111: "sepolia",
  137: "polygon",
  250: "fantom",
  43114: "avalanche",
  1666600000: "harmony",
  42161: "arbitrum",
  1284: "moonbeam",
  56: "Binance Smart Chain",
  53935: "DFK Chain",
  204: "opBNB Mainnet",
  324: "zkSync Mainnet"
}

const NETWORK_PREFIXES = {
  "eth": 1,
  "ropsten": 3,
  "rinkeby": 4,
  "goerli" : 5,
  "kovan": 42,
  "sepolia": 11155111,
  "matic": 137,
  "ftm": 250,
  "avax": 43114,
  "one": 1666600000,
  "arb": 42161,
  "glmr": 1284,
  "bsc": 56,
  "dfk": 53935,
  "opbnb": 204,
  "zksync": 324
}

const _capitilize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const getNetworkName = (networkId) => {
  return NETWORKS[networkId]
}

const getNetworkFromPrefix = (prefix) => {
  return NETWORK_PREFIXES[prefix]
}

const parseNetworkAddress = (address) => {
  const chunks = address.toLowerCase().split(':')
  if (chunks.length != 2) return [null, null]
  
  const network = getNetworkFromPrefix(chunks[0])
  return [network, chunks[1]]
}

const hexlifyNetworkId = (networkId) => utils.hexValue(networkId)

const polygonConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(137)),
    chainId: hexlifyNetworkId(137),
    nativeCurrency: {
      name: 'POL',
      symbol: 'POL',
      decimals: 18,
    },
    rpcUrls: ['https://polygon-rpc.com'],
    blockExplorerUrls: ['https://polygonscan.com'],
  }
}

const harmonyOneConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(1666600000)),
    chainId: hexlifyNetworkId(1666600000),
    nativeCurrency: {
      name: 'ONE',
      symbol: 'ONE',
      decimals: 18,
    },
    rpcUrls: ['https://api.harmony.one'],
    blockExplorerUrls: ['https://explorer.harmony.one/'],
  }
}

const bscConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(56)),
    chainId: hexlifyNetworkId(56),
    nativeCurrency: {
      name: 'BNB',
      symbol: 'BNB',
      decimals: 18,
    },
    rpcUrls: ['https://bsc-dataseed.binance.org'],
    blockExplorerUrls: ['https://bscscan.com'],
  }
}

const opbnbConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(204)),
    chainId: hexlifyNetworkId(204),
    nativeCurrency: {
      name: 'BNB',
      symbol: 'BNB',
      decimals: 18,
    },
    rpcUrls: ['https://opbnb-mainnet-rpc.bnbchain.org'],
    blockExplorerUrls: ['https://mainnet.opbnbscan.com'],
  }
}

const dfkConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(53935)),
    chainId: hexlifyNetworkId(53935),
    nativeCurrency: {
      name: 'JEWEL',
      symbol: 'JEWEL',
      decimals: 18,
    },
    rpcUrls: ['https://subnets.avax.network/defi-kingdoms/dfk-chain/rpc'],
    blockExplorerUrls: ['https://subnets.avax.network/defi-kingdoms/'],
  }
}

const zksyncConfig = () => {
  return {
    chainName: _capitilize(getNetworkName(324)),
    chainId: hexlifyNetworkId(324),
    nativeCurrency: {
      name: 'ETH',
      symbol: 'ETH',
      decimals: 18,
    },
    rpcUrls: ['https://mainnet.era.zksync.io'],
    blockExplorerUrls: ['https://explorer.zksync.io'],
  }
}

const configForNetwork = (networkId) => {
  switch (networkId) {
    case 56:
      return bscConfig()

    case 137:
      return polygonConfig()

    case 1666600000:
      return harmonyOneConfig()

    case 53935:
      return dfkConfig()

    case 204:
      return opbnbConfig()

    case 324:
      return zksyncConfig()

    default:
      return null
  }
}

export {
  configForNetwork,
  getNetworkName,
  getNetworkFromPrefix,
  parseNetworkAddress,
  hexlifyNetworkId
}
