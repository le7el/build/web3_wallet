/*
 * Utilities for errors processing
 */ 
import { decodeError } from 'ethers-decode-error'
 
const parseWalletErrorMessage = (error, abi = null) => {
  let name, msg

  if (typeof error === 'string') msg = error
  else if (error.data && error.data.message) msg = error.data.message
  else if (error.reason) msg = error.reason
  else if (error instanceof Error) {
    name = error.name
    msg = error.message
  }
  else if (error.message) msg = error.message
  else if (error.error && error.error.reason) msg = error.error.reason
  else if (error.error && error.error.message) msg = error.error.message
  else msg = 'Something went wrong, please try again'

  if (msg === 'execution reverted' && abi !== null) {
    let decoded = decodeError(error, abi)
    if (decoded.type != 1) {
      msg = decoded.error
      name = decoded.type
    }
  }

  msg = parseUserError(msg)

  return [msg, name, error]
}

const defaultFailCallback = (error) => {
  const [msg, name] = parseWalletErrorMessage(error)

  console.error('Failed to setup wallet', msg, name, error)
}

const parseUserError = function(error){
  let res, 
    i = 0,
    expressions = [
      /^cannot\sestimate\sgas.+error=\{"code":[^\,]+\,"message":"([^"]+)"\,.+/,
      /^cannot\sestimate\sgas.+\(reason="([^"]+)"\,.+/,
      /^(call\srevert\sexception)\s\[.+/,
      /(processing\sresponse\serror).+jsonrpc.+2\.0.+id.+error.+code.+\-32000.+message.+transaction\sun.+/,
    ];

  do {
    res = error.match(expressions[i]);
    i++;
  } while (!res);

  return res[0] ? res[1] : error
}

class NoAccountsConnectedError extends Error {
  constructor(...params) {
    super(...params)

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, NoAccountsConnectedError)
    }

    this.name = 'NoAccountsConnectedError'
  }
}

const isMetamaskAddNetworkFirstError = (error) => {
  if (error && error.code === 4902)
    return true

  // mobile mm browser wraps original error
  if (error && error.data?.originalError?.code === 4902)
    return true

  return false
}

export {
  parseWalletErrorMessage,
  defaultFailCallback,
  NoAccountsConnectedError,
  isMetamaskAddNetworkFirstError,
}
