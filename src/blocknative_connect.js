import Onboard from '@web3-onboard/core'
import injectedModule from '@web3-onboard/injected-wallets'
import walletConnectModule from '@web3-onboard/walletconnect'
import ledgerModule from '@web3-onboard/ledger'
import trezorModule from '@web3-onboard/trezor'
import coinbaseWalletModule from '@web3-onboard/coinbase'

const MAINNET_RPC_URL = 'https://ethereum-rpc.publicnode.com'
const SEPOLIA_RPC_URL = 'https://ethereum-sepolia-rpc.publicnode.com'
const POLYGON_RPC_URL = 'https://polygon-rpc.com'

const wcInitOptions = {
  version: 2,
  /**
   * Project ID associated with [WalletConnect account](https://cloud.walletconnect.com)
   */
  projectId: 'df59f355994524313ec1398a886d42d2',
  /**
   * Chains requireds to be supported by all wallets connecting to your DApp
   */
  requiredChains: [137],
  /**
   * Chains required to be supported by all wallets connecting to your DApp
   */
  //optionalChains: [137],
  /**
   * Defaults to `appMetadata.explore` that is supplied to the web3-onboard init
   * Strongly recommended to provide atleast one URL as it is required by some wallets (i.e. MetaMask)
   * To connect with WalletConnect
   */
  dappUrl: 'https://le7el.com',
}

const ledgerOptions = {
  walletConnectVersion: 2,
  /**
   * Project ID associated with [WalletConnect account](https://cloud.walletconnect.com)
   */
  projectId: 'df59f355994524313ec1398a886d42d2',
  /**
   * Chains required to be supported by all wallets connecting to your DApp
   */
  requiredChains: [137],
  /**
   * Chains required to be supported by all wallets connecting to your DApp
   */
  //optionalChains: [1, 137],
}

const trezorOptions = {
  email: 'info@le7el.com',
  appUrl: 'https://le7el.com'
}

const injected = injectedModule()
const walletConnect = walletConnectModule(wcInitOptions)
const ledger = ledgerModule(ledgerOptions)
const trezor = trezorModule(trezorOptions)
const coinbase = coinbaseWalletModule()

const initOnboard = (autoConnectLastWallet = true) => {
  return Onboard({
    appMetadata: {
      name: 'LE7EL',
      icon: 'https://le7el.com/images/icons/safari-pinned-tab.svg',
      description: 'The Apex Gaming Protocol. Built, operated and owned by the community.🏁'
    },
    connect: {
      autoConnectLastWallet: autoConnectLastWallet
    },
    wallets: [
      injected,
      walletConnect,
      coinbase,
      ledger,
      trezor
    ],
    chains: [
      {
        id: '0x89',
        token: 'POL',
        label: 'Polygon',
        rpcUrl: POLYGON_RPC_URL,
        blockExplorerUrl: 'https://polygonscan.com'
      },
      {
        id: '0xaa36a7',
        token: 'ETH',
        label: 'Sepolia',
        rpcUrl: SEPOLIA_RPC_URL,
        blockExplorerUrl: 'https://sepolia.etherscan.io'
      }
    ]
  })
}

const blocknativeConnect = (autoConnectLastWallet = true) => {
  const onboard = initOnboard(autoConnectLastWallet)
  return onboard.connectWallet()
}

export {
  MAINNET_RPC_URL,
  POLYGON_RPC_URL,
  SEPOLIA_RPC_URL,
  initOnboard,
  blocknativeConnect
}