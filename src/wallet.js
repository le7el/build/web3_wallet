import {
  setLocal,
  isMetamask,
  isBrowserWallet,
  isWalletConnect,
  isBlocknative,
  resetLocal,
  getLocal,
  removeLocal,
  currentAddress,
  currentNetwork,
} from './local'
import { getOrigin, checkIsIframe, notifyAccountsChanged, listenAccountsChanged, notifyMetamaskDeepLink } from './cors'
import { defaultFailCallback, NoAccountsConnectedError, isMetamaskAddNetworkFirstError } from './errors'
import { configForNetwork, getNetworkName, hexlifyNetworkId } from './networks'
import { getEthersProvider, toUtf8Bytes, resetEthersProvider } from './ethers'

const _onAccountsChanged = (accounts) => {
  if (accounts.length === 0) {
    removeLocal('wallet')
    return Promise.reject(new NoAccountsConnectedError('Please connect to your wallet.'))
  }

  const account = accounts[0].toLowerCase()
  setLocal('wallet', account)

  return Promise.resolve(account)
}

const _listenToChanges = (callback, failCallback) => {
  const origin = getOrigin()
  console.debug(`[${origin}] listen for web3 changes`)

  const handleAccountsChanged = function(accounts) {
    console.debug(`[${origin}] accountsChanged`, accounts)
    _onAccountsChanged(accounts)
      .then(account => callback(account, currentNetwork()))
      .catch(failCallback)

    if (checkIsIframe()) {
      notifyAccountsChanged(window.parent)
    }
  }

  const handleChainChanged = function(chainId) {
    console.debug(`[${origin}] chainChanged`, chainId)
    setLocal('selectedNetwork', chainId)
    callback(currentAddress(), currentNetwork())
  }

  let cleanupListeners = () => { console.error("No cleanup attached") }
  getEthersProvider(true)
    .then((ethersProvider) => {
      const { provider: providerEngine } = ethersProvider
      cleanupListeners = () => {
        console.log("Cleanup Listeners")
        providerEngine.removeListener('accountsChanged', handleAccountsChanged)
        providerEngine.removeListener('chainChanged', handleChainChanged)
      }
    
      providerEngine.on('accountsChanged', handleAccountsChanged)
      providerEngine.on('chainChanged', handleChainChanged)

      listenAccountsChanged(() => {
        ethersProvider.send('eth_accounts').then((accounts) => {
          _onAccountsChanged(accounts)
            .then(account => callback(account, currentNetwork))
            .catch(failCallback)
        })
      })

      return cleanupListeners
    })
  
  return cleanupListeners
}

const _connect = () => {
  return getEthersProvider(true)
    .then((provider) => provider.send('eth_requestAccounts'))
}

const connectWallet = () => setupWallet(true)
const getWallet = () => setupWallet(false)
const listenWallet = (callback, failCallback = defaultFailCallback) => _listenToChanges(callback, failCallback)

const setupWallet = (doAutoconnect = true) => {
  const ethersSetup = () => {
    return getEthersProvider(true, false)
      .then((provider) => {
        const p1 = provider.send('eth_accounts')
          .then((accounts) => {
            if (accounts.length === 0 && doAutoconnect) {
              return _connect()
            }

            return accounts
          })
          .then(accounts => _onAccountsChanged(accounts))

        const p2 = provider.send('eth_chainId')
          .then((chainId) => setLocal('selectedNetwork', chainId))

        // listen only after all settled (maybe all() is better), cause some providers (like walletconnect)
        // could emit web3 events on initial calls to 'eth_accounts', 'eth_chainId', 'eth_requestAccounts'
        return Promise.allSettled([p1, p2]).then(([res_p1, _res_p2]) => {
          if (res_p1.status === 'fulfilled') {
            return Promise.resolve(res_p1.value)
          }

          return Promise.reject(res_p1.reason)
        })
      })
  }

  if (isBlocknative()) {
    return ethersSetup()
      .catch(error => {
        resetEthersProvider()
        throw error
      })
  } else if (isMetamask() || isBrowserWallet()) {
    return ethersSetup()
  } else if (isWalletConnect()) {
    if (doAutoconnect) {
      const { provider: providerEngine } = ethersProvider
      return providerEngine.enable()
        .then(ethersSetup).catch(error => {
          resetEthersProvider()
          throw error
        })
    }

    // autoconnection disabled, don't show connect interface and just return connected wallet if there is one
    const accounts = currentAddress() ? [currentAddress()] : []
    return _onAccountsChanged(accounts)
  } else {
    console.error('Unsupported web3 provider')
    return Promise.reject('Unsupported web3 provider')
  }
}

const _getWalletConnectSignMsg = (providerEngine) => {
  if (providerEngine.wc && providerEngine.wc._peerMeta && providerEngine.wc._peerMeta.description)
    return `Sign with your wallet connect app: ${providerEngine.wc._peerMeta.description}`

  return 'Go to our wallet app to sign operation'
}

const signWallet = (ethWallet, challenge, onSignatureStart = (msg = null) => { }, onSignatureEnd = () => { }) => {
  const message = toUtf8Bytes(challenge)
  return getEthersProvider(true)
    .then((ethersProvider) => {
      const { provider: providerEngine } = ethersProvider
      const msg = isWalletConnect() ? _getWalletConnectSignMsg(providerEngine) : null
      onSignatureStart(msg)

      const signer = ethersProvider.getSigner(ethWallet)
      return signer.signMessage(message).finally(onSignatureEnd)
    })
}

const shortenedWallet = function(wallet) {
  if (!wallet || wallet.length < 11) {
    return ''
  }

  return `${wallet.substr(0, 6)}...${wallet.substr(wallet.length - 4)}`
}

const resetWallet = () => {
  resetEthersProvider()
  resetLocal()
}

/**
 * wallet_addEthereumChain suggests user to add and then to switch network, there are 2 outcomes:
 * 1) If user approved to switch to new network, on wallet_addEthereumChain.then
 *    can be a race condition when chain is not changed yet for provider. 
 *    It is solved by starting ot listen wallet for chain changes.
 * 2) If user added network, but declined to swicth to it, there won't be any errors in wallet_addEthereumChain.catch
 *    It is solved with timeout.
 */
const _addAndChangeNetwork = (networkId, defaultConfig) => {
  let changeNetworkTimeoutId
  return new Promise((resolve, reject) => {
    const cleanupListeners = _listenToChanges((curAcc, curChainId) => {
      if (curChainId === networkId) {
        cleanupListeners()
        clearTimeout(changeNetworkTimeoutId)
        resolve(curChainId)
      }
    })

    getEthersProvider(true)
      .then(provider => provider.send('wallet_addEthereumChain', [defaultConfig]))
      .then(() => {
        changeNetworkTimeoutId = setTimeout(() => {
          cleanupListeners()
          reject('Failed to change network, please try again')
        }, 1000)
      })
      .catch(error => {
        cleanupListeners()
        reject(error)
      })
  })
}

const changeNetwork = (networkId, enforce = false) => {
  if (currentNetwork() === networkId) return Promise.resolve(networkId)
  if (!isMetamask() && !isBrowserWallet()) {
    if (enforce) {
      return Promise.reject(`please, switch to ${getNetworkName(networkId)} in your wallet app and try again`)
    } else {
      return Promise.reject('changeNetwork method is not supported in a current wallet')
    }
  }
  const hexNetworkId = hexlifyNetworkId(networkId)

  return getEthersProvider(true)
    .then(provider => provider.send('wallet_switchEthereumChain', [{ chainId: hexNetworkId }]))
    .catch((error) => {
      const defaultConfig = configForNetwork(networkId)
      if (isMetamaskAddNetworkFirstError(error) && defaultConfig) {
        return _addAndChangeNetwork(networkId, defaultConfig)
      }

      return Promise.reject(error)
    })
    .then(() => {
      setLocal('selectedNetwork', networkId)

      return Promise.resolve(networkId)
    })
}

const useProvider = (provider) => {
  const currentProvider = getLocal('provider')
  if (currentProvider && currentProvider !== provider) {
    resetEthersProvider()
  }

  setLocal('provider', provider)
  if (provider === 'metamask' && !isMetamask()) {
    if (isBrowserWallet()) return Promise.resolve()
    openWithMetamaskDeepLink()
    const msg = 'web3 provider is not available in current scope, trying to call it with deeplink'
    console.warn(msg)
    return Promise.reject('web3 provider is not available in current scope, trying to call it with deeplink')
  }

  return Promise.resolve()
}

const openWithMetamaskDeepLink = (deepLinkBaseUrl = 'https://metamask.app.link/dapp/') => {
  if (checkIsIframe()) {
    // top parent should redirect to deeplink, only in this case mobile os will correctly open app
    notifyMetamaskDeepLink(window.parent, deepLinkBaseUrl)
    return;
  }

  const currentLocationUrl = new URL(window.location.href)
  const dappUrl = currentLocationUrl.href.substr(`${currentLocationUrl.protocol}//`.length, currentLocationUrl.href.length)

  window.location.href = `${deepLinkBaseUrl}${dappUrl}`
}

export {
  useProvider,
  setupWallet,
  connectWallet,
  getWallet,
  listenWallet,
  signWallet,
  resetWallet,
  shortenedWallet,
  changeNetwork,
  openWithMetamaskDeepLink,
}