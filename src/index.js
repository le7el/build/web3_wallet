import {
  useProvider,
  setupWallet,
  getWallet,
  connectWallet,
  signWallet,
  resetWallet,
  listenWallet,
  shortenedWallet,
  changeNetwork,
  openWithMetamaskDeepLink,
} from './wallet'
import {
  currentNetwork,
  currentNetworkName,
  currentAddress,
  isLocalUrl,
  hasPrevConnectWalletSession,
  isMetamask,
  isBrowserWallet,
  isWalletConnect,
} from './local'
import { notifyAccountsChanged, getOrigin } from './cors'
import { parseWalletErrorMessage } from './errors'
import { cidToBytes32, bytes32ToCid } from './ipfs'
import { getNetworkFromPrefix, parseNetworkAddress } from './networks'
import { 
  getEthersProvider,
  getCanonicReadOnlyProvider,
  parseTokenAmount,
  isValidWallet,
  resetEthersProvider,
} from './ethers'

export {
  useProvider,
  setupWallet,
  connectWallet,
  signWallet,
  resetWallet,
  resetEthersProvider,
  getWallet,
  listenWallet,
  hasPrevConnectWalletSession,
  shortenedWallet,
  changeNetwork,
  currentNetwork,
  currentNetworkName,
  currentAddress,
  notifyAccountsChanged,
  isLocalUrl,
  parseWalletErrorMessage,
  cidToBytes32,
  bytes32ToCid,
  getNetworkFromPrefix,
  parseNetworkAddress,
  getEthersProvider,
  getCanonicReadOnlyProvider,
  parseTokenAmount,
  isValidWallet,
  isMetamask,
  isBrowserWallet,
  isWalletConnect,
  openWithMetamaskDeepLink,
}