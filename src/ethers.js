import { ethers, utils } from "ethers"
import { isMetamask, isBrowserWallet, isWalletConnect, isBlocknative } from './local'
import { blocknativeConnect } from './blocknative_connect'

let provider

const POLYGON_RPC_URLS = [
  'https://polygon-rpc.com',
  'https://1rpc.io/matic',
  'https://rpc-mainnet.matic.network',
  'https://matic-mainnet.chainstacklabs.com'
];

const getEthersProvider = (returnPromise = false, autoConnectLastWallet = true) => {
  if (returnPromise) {
    return Promise.resolve(getEthersProviderRaw(autoConnectLastWallet))
  }
  return getEthersProviderRaw(autoConnectLastWallet)
}

const getEthersProviderRaw = (autoConnectLastWallet = true) => {
  if (isBlocknative()) {
    if (!provider) {
      return blocknativeConnect(autoConnectLastWallet)
        .then((wallets) => {
          provider = new ethers.providers.Web3Provider(wallets[0].provider, 'any');
          return provider
        })
    }
    return provider
  } else if (isMetamask() || isBrowserWallet()) {
    if (!provider || provider.provider !== window.ethereum) {
      provider = new ethers.providers.Web3Provider(window.ethereum, 'any')
    }
    return provider
  } else if (isWalletConnect()) {
    if (!provider || !provider.provider instanceof WalletConnectProvider) {
      const options = {
        rpc: {
          137: "https://polygon-rpc.com"
        },
        infuraId: '23801dec8bb948e59505b3141f4b8654',
        bridge: document.location.host === 'le7el.com' ? 'wss://wc.le7el.com' : 'wss://wc.demo.le7el.com'
      }

      if (['iPad Simulator', 'iPhone Simulator', 'iPad', 'iPhone'].includes(navigator.platform)) {
        options['qrcodeModalOptions'] = {
          mobileLinks: [
            "trust",
            "argent",
            "rainbow",
            "imtoken",
            "pillar"
          ]
        }
      }

      const wcProvider = new WalletConnectProvider(options)
      provider = new ethers.providers.Web3Provider(wcProvider, 'any')
    }
    return provider
  } else {
    return null
  }
}

const getCanonicReadOnlyProvider = (prod = false) => {
  if (prod) {
    return new ethers.providers.StaticJsonRpcProvider('https://polygon.rpc.grove.city/v1/542933d5', 137)
  } else {
    return new ethers.providers.StaticJsonRpcProvider('https://eth-sepolia.g.alchemy.com/v2/F3QzosmQcmXGyTugnnXGFQCCWi3kHvaj', 11155111)
  }
}

const parseTokenAmount = (amount, decimals) => {
  if (!decimals) return ethers.utils.formatUnits(amount)
  return ethers.utils.formatUnits(amount, decimals)
}

const isValidWallet = function(walletEthAddress) {
  return walletEthAddress?.match(/^0x[a-fA-F0-9]{40}$/i) && ethers.utils.isAddress(walletEthAddress);
}

const resetEthersProvider = function() {
  provider = null
}

const { toUtf8Bytes } = utils

export {
  getEthersProvider,
  getCanonicReadOnlyProvider,
  parseTokenAmount,
  isValidWallet,
  toUtf8Bytes,
  resetEthersProvider,
}