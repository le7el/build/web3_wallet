/*
  Utilities for for local configuration
*/
import { getNetworkName } from "./networks"

const currentNetwork = () => {
  const selectedNetwork = getLocal('selectedNetwork') || '1'
  if (typeof selectedNetwork === 'string') {
    return parseInt(selectedNetwork)
  }
  return selectedNetwork
}

/**
 * @returns string || undefined
 */
const currentNetworkName = () => {
  return getNetworkName(currentNetwork())
}

const currentAddress = () => {
  const wallet = getLocal('wallet')
  if (typeof wallet === 'string') {
    return wallet.toLowerCase()
  }
  return wallet
}

const isMetamask = () => {
  return (
    getLocal('provider') !== 'walletconnect' &&
    typeof window.ethereum !== 'undefined' &&
    typeof window.ethereum.isMetaMask !== 'undefined' &&
    window.ethereum.isMetaMask === true
  )
}

const isBrowserWallet = () => {
  return (
    getLocal('provider') !== 'walletconnect' &&
    typeof window.ethereum !== 'undefined' &&
    (typeof window.ethereum.isMetaMask === 'undefined' || window.ethereum.isMetaMask === false)
  )
}

const isWalletConnect = () => {
  return getLocal('provider') === 'walletconnect'
}

const isBlocknative = () => {
  return getLocal('provider') === 'blocknative'
}

const isLocalUrl = (url) => {
  try {
    const urlObj = new URL(url)
    const hostname = urlObj.hostname

    return hostname.endsWith('localhost') || hostname.endsWith('local')
  } catch (e) {
    if (typeof url === 'string' && url.startsWith('/')) {
      return true
    }

    return false
  }
}

const setLocal = (key, value) => {
  localStorage.setItem(key, value)
}

const getLocal = (key, defaultValue = null) => {
  const item = localStorage.getItem(key)
  if (item === null) return defaultValue
  if (item === 'null') return null
  if (item === 'true') return true
  if (item === 'false') return false

  return item
}

const removeLocal = (key) => {
  return localStorage.removeItem(key)
}

const resetLocal = () => {
  removeLocal('selectedNetwork')
  removeLocal('wallet')
  removeLocal('provider')
  resetWalletConnect()
  resetBlocknativeConnect()
}

const resetWalletConnect = () => {
  removeLocal('walletconnect')
}

const resetBlocknativeConnect = () => {
  removeLocal('onboard.js:agreement')
  removeLocal('onboard.js:last_connected_wallet')
}

const hasPrevConnectWalletSession = () => getLocal('provider') !== null

export {
  isLocalUrl,
  isMetamask,
  isBrowserWallet,
  isBlocknative,
  isWalletConnect,
  currentAddress,
  currentNetwork,
  currentNetworkName,
  setLocal,
  removeLocal,
  getLocal,
  resetLocal,
  hasPrevConnectWalletSession,
}
