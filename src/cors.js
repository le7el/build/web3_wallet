/*
  Utilities for cross origin interaction ( iframes )
*/

const getOrigin = (windowEl = window) => {
  return windowEl.location.origin + windowEl.location.pathname
}

/**
 * Notifies any window about metamaskAccountChanged winthout including sensitive data
 * Receiver should listen (see `listenAccountsChanged()`) to this event and request Metamask for new accounts
 * 
 * It's usefull for any iframe to notify parent, which happens automatically in `wallet/_listenToChanges()`.
 * It can be usefull for some parent, which are including iframes to notify some of them, example:
 * ```
 * const doSyncAccounts = () => {
 *   iframes.forEach((iframe) => {
 *     if (shouldBeNotified(iframe.src))
 *       notifyAccountsChanged(iframe.contentWindow)
 *   })
 * }
 * ```
 *  
 * @param {window} receiver 
 */
const notifyAccountsChanged = (receiver) => {
  const event = {
    method: 'metamaskAccountsChanged',
    payload: { origin: getOrigin() },
  }
  receiver.postMessage(event, '*')
}

/**
 * @param {Window} receiver 
 * @param {string} baseUrl 
 */
const notifyMetamaskDeepLink = (receiver, baseUrl) => {
  // top parent should redirect to deeplink, only in this case mobile os will correctly open app
  const event = {
    method: 'handleMetamaskDeepLink',
    payload: {
      baseUrl: baseUrl,
    },
  }

  receiver.postMessage(event, "*")
}

/**
 * Metamask doesn't emit accountsChanged event for different hostnames (even for subdomains).
 * Start listening for event on every origin (parent and iframe childs)
 * and apply account changed only if event origin differs
 */
const listenAccountsChanged = (callback = () => {}) => {
  window.addEventListener('message', (event) => {
    if (!event.data) {
      return
    }

    switch (true) {
      case typeof event.data === 'object' && event.data.method === 'metamaskAccountsChanged':
        if (event.data.payload.origin !== getOrigin()) {
          callback()

          // when message received from parent do not post it back
          if (checkIsIframe() && event.data.payload.origin !== getOrigin(window.parent)) {
            window.parent.postMessage(event.data, '*')
          }
        }
        return
      default:
        return
    }
  })
}

const checkIsIframe = () => {
  return window.parent && window.parent !== window
}

export {
  notifyAccountsChanged,
  getOrigin,
  checkIsIframe,
  listenAccountsChanged,
  notifyMetamaskDeepLink
}
